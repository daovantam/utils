#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE house_rental;
    CREATE DATABASE info_service;
    CREATE DATABASE file_service;
    GRANT ALL PRIVILEGES ON DATABASE house_rental TO postgres;
    GRANT ALL PRIVILEGES ON DATABASE info_service TO postgres;
    GRANT ALL PRIVILEGES ON DATABASE file_service TO postgres;
EOSQL
